#########################################################################
#
# Anomali CONFIDENTIAL
# __________________
#
#  Copyright 2016 Anomali Inc.
#  All Rights Reserved.
#
# NOTICE:  All information contained herein is, and remains
# the property of Anomali Incorporated and its suppliers,
# if any.  The intellectual and technical concepts contained
# herein are proprietary to Anomali Incorporated
# and its suppliers and may be covered by U.S. and Foreign Patents,
# patents in process, and are protected by trade secret or copyright law.
# Dissemination of this information or reproduction of this material
# is strictly forbidden unless prior written permission is obtained
# from Anomali Incorporated.
#
#########################################################################


import os
import sys
import requests

from AnomaliTransform import AnomaliTransform
from AnomaliTransform import EntityTypes
from dateutil.parser import parse
import datetime
import re


api_base = "https://api.passivetotal.org/v2/"
username = None
key = None


def domainToIP(at, search_string):
    try:
        response = requests.get(url=api_base + 'dns/passive?query=%s' % search_string, auth=(username, key))
        response_json = response.json()
        resp_code = response.status_code
        if resp_code == 200:
            if 'results' in response_json:
                for resolution in response_json['results']:
                    recordType = resolution['recordType']
                    resolveType = resolution['resolveType']
                    if recordType == 'A' and resolveType == 'ip':
                        ae = at.addEntity(EntityTypes.IPv4, '%s' % resolution['resolve'])
                        ae.addAdditionalField('lastSeen', 'Last Resolved', '%s' % resolution['lastSeen'])
        else:
            response.raise_for_status()
    except requests.exceptions.HTTPError as e:
        status_code = e.response.status_code
        if status_code == 401:
            at.addMessage('WARNING', 'Your credentials have expired')
        elif status_code == 500:
            at.addMessage('WARNING', 'The server encountered an unexpected error')
        else:
            at.addMessage('WARNING', 'Unknown error occurred, please try again')
    except:
        at.addException('domainToIP Unknown Error:%sType: %s%sValue:%s' %
                        (os.linesep, sys.exc_info()[0], os.linesep, sys.exc_info()[1]))
    return at


def emailToDomain(at, search_string):
    try:
        response = requests.get(url=api_base + 'whois/search?query=%s&field=email' % search_string,
                                auth=(username, key))
        response_json = response.json()
        resp_code = response.status_code
        if resp_code == 200:
            if 'results' in response_json:
                for each_domain in response_json['results']:
                    ent_type_address = bool(re.search(r"^\d{1,3}\.\d{1,3}\.\d{1,3}\.\d{1,3}$", each_domain['domain']))
                    ent_type_domain = bool(re.search(r"^([0-9a-z][-\w]*[0-9a-z]\.)+[a-z0-9\-]{2,15}$", each_domain['domain']))
                    if ent_type_address is True:
                        ae = at.addEntity(EntityTypes.IPv4, '%s' % each_domain['domain'])
                    elif ent_type_domain is True:
                        ae = at.addEntity(EntityTypes.Domain, '%s' % each_domain['domain'])
                    elif '@' in each_domain['domain']:
                        ae = at.addEntity(EntityTypes.EmailAddress, '%s' % each_domain['domain'])
                    else:
                        ae = at.addEntity(EntityTypes.Phrase, '%s' % each_domain['domain'])
                    ae.addAdditionalField('organization', 'Organization', '%s' % each_domain.get('organization', '')
                                          or 'no data')
        else:
            response.raise_for_status()
    except requests.exceptions.HTTPError as e:
        status_code = e.response.status_code
        if status_code == 401:
            at.addMessage('WARNING', 'Your credentials have expired')
        elif status_code == 500:
            at.addMessage('WARNING', 'The server encountered an unexpected error')
        else:
            at.addMessage('WARNING', 'Unknown error occurred, please try again')
    except:
        at.addException('emailToDomain Unknown Error:%sType: %s%sValue:%s' %
                        (os.linesep, sys.exc_info()[0], os.linesep, sys.exc_info()[1]))
    return at


def domainToChildHostPairs(at, search_string):
    try:
        response = requests.get(url=api_base + 'host-attributes/pairs?query=%s&direction=children' % search_string,
                                auth=(username, key))
        response_json = response.json()
        resp_code = response.status_code
        if resp_code == 200:
            for each_pair in response_json['results']:
            # for each_pair in list(response_json['results'])[0:10]:
                ae = at.addEntity(EntityTypes.Domain, '%s' % each_pair['child'])
                ae.addAdditionalField('parent', 'Parent', '%s' % each_pair['parent'])
                ae.addAdditionalField('child', 'Child', '%s' % each_pair['child'])
                ae.addAdditionalField('cause', 'Cause', '%s' % each_pair['cause'])
                ae.addAdditionalField('lastSeen', 'Last Seen', '%s' % each_pair['lastSeen'])
        else:
            response.raise_for_status()
    except requests.exceptions.HTTPError as e:
        status_code = e.response.status_code
        if status_code == 401:
            at.addMessage('WARNING', 'Your credentials have expired')
        elif status_code == 500:
            at.addMessage('WARNING', 'The server encountered an unexpected error')
        else:
            at.addMessage('WARNING', 'Unknown error occurred, please try again')
    except:
        at.addException('domainToChildHostpairs Unknown Error:%sType: %s%sValue:%s' %
                        (os.linesep, sys.exc_info()[0], os.linesep, sys.exc_info()[1]))
    return at


def domainToParentHostPairs(at, search_string):
    try:
        response = requests.get(url=api_base + 'host-attributes/pairs?query=%s&direction=parents' % search_string,
                                auth=(username, key))
        response_json = response.json()
        resp_code = response.status_code
        if resp_code == 200:
            for each_pair in response_json['results']:
                ae = at.addEntity(EntityTypes.Domain, '%s' % each_pair['parent'])
                ae.addAdditionalField('parent', 'Parent', '%s' % each_pair['parent'])
                ae.addAdditionalField('child', 'Child', '%s' % each_pair['child'])
                ae.addAdditionalField('cause', 'Cause', '%s' % each_pair['cause'])
                ae.addAdditionalField('lastSeen', 'Last Seen', '%s' % each_pair['lastSeen'])
        else:
            response.raise_for_status()
    except requests.exceptions.HTTPError as e:
        status_code = e.response.status_code
        if status_code == 401:
            at.addMessage('WARNING', 'Your credentials have expired')
        elif status_code == 500:
            at.addMessage('WARNING', 'The server encountered an unexpected error')
        else:
            at.addMessage('WARNING', 'Unknown error occurred, please try again')
    except:
        at.addException('domainToParentHostPairs Unknown Error:%sType: %s%sValue:%s' %
                        (os.linesep, sys.exc_info()[0], os.linesep, sys.exc_info()[1]))
    return at


def ipToDomain(at, search_string):
    try:
        response = requests.get(url=api_base + 'dns/passive?query=%s' % search_string, auth=(username, key))
        response_json = response.json()
        resp_code = response.status_code
        if resp_code == 200:
            if 'results' in response_json:
                for resolution in response_json['results']:
                    recordType = resolution['recordType']
                    resolveType = resolution['resolveType']
                    if recordType == 'A' and resolveType == 'domain':
                        ae = at.addEntity(EntityTypes.Domain, '%s' % resolution['resolve'])
                        ae.addAdditionalField('lastSeen', 'Last Resolved', '%s' % resolution['lastSeen'])
        else:
            response.raise_for_status()
    except requests.exceptions.HTTPError as e:
        status_code = e.response.status_code
        if status_code == 401:
            at.addMessage('WARNING', 'Your credentials have expired')
        elif status_code == 500:
            at.addMessage('WARNING', 'The server encountered an unexpected error')
        else:
            at.addMessage('WARNING', 'Unknown error occurred, please try again')
    except:
        at.addException('ipToDomain Unknown Error:%sType: %s%sValue:%s' %
                        (os.linesep, sys.exc_info()[0], os.linesep, sys.exc_info()[1]))
    return at


def ipToSSL(at, search_string):
    try:
        data = {'query': search_string, 'field': 'SubjectCommonName'}
        response = requests.get(url=api_base + 'ssl-certificate/search',
                                auth=(username, key), json=data)
        response_json = response.json()
        resp_code = response.status_code
        if resp_code == 200:
            if 'results' in response_json:
                for each_cert in response_json['results']:
                    ae = at.addEntity(EntityTypes.Hash, '%s' % each_cert.get('sha1', ''))
                    ae.addAdditionalField('subjectCommonName', 'Subject Common Name', each_cert.get('subjectCommonName', 'no data') or 'no data')
                    ae.addAdditionalField('subjectCountry', 'Subject Country', each_cert.get('subjectCountry', 'no data') or 'no data')
                    ae.addAdditionalField('issuerCommonName', 'Issuer Common Name', each_cert.get('issuerCommonName', 'no data') or 'no data')
                    ae.addAdditionalField('issuerCountry', 'Issuer Country', each_cert.get('issuerCountry', 'no data') or 'no data')
                    ae.addAdditionalField('issueDate', 'Issue Date', parse(each_cert.get('issueDate', '')).strftime("%Y-%m-%d") or 'no data')
                    ae.addAdditionalField('expirationDate', 'Expiration Date', parse(each_cert.get('expirationDate', '')).strftime("%Y-%m-%d") or 'no data')
                    ae.addAdditionalField('firstSeen', 'First Seen', datetime.datetime.fromtimestamp(each_cert.get('firstSeen', '') // 1000).strftime("%Y-%m-%d") or 'no data')
                    ae.addAdditionalField('lastSeen', 'Last Seen', datetime.datetime.fromtimestamp(each_cert.get('lastSeen', '') // 1000).strftime("%Y-%m-%d") or 'no data')
                    # ae.addAdditionalField('firstSeen', 'First Resolved', '%s' % each_cert['firstSeen'])
                    # ae.addAdditionalField('lastSeen', 'Last Resolved', '%s' % each_cert['lastSeen'])
        else:
            response.raise_for_status()
    except requests.exceptions.HTTPError as e:
        status_code = e.response.status_code
        if status_code == 401:
            at.addMessage('WARNING', 'Your credentials have expired')
        elif status_code == 500:
            at.addMessage('WARNING', 'The server encountered an unexpected error')
        else:
            at.addMessage('WARNING', 'Unknown error occurred, please try again')
    except:
        at.addException('ipToSSL Unknown Error:%sType: %s%sValue:%s' %
                        (os.linesep, sys.exc_info()[0], os.linesep, sys.exc_info()[1]))
    return at


def domainToSSL(at, search_string):
    try:
        data = {'query': search_string, 'field': 'SubjectCommonName'}
        response = requests.get(url=api_base + 'ssl-certificate/search',
                                auth=(username, key), json=data)
        response_json = response.json()
        resp_code = response.status_code
        if resp_code == 200:
            if 'results' in response_json:
                for each_cert in response_json['results']:
                    ae = at.addEntity(EntityTypes.Hash, '%s' % each_cert.get('sha1', 'no data') or 'no data')
                    ae.addAdditionalField('subjectCommonName', 'Subject Common Name',
                                          each_cert.get('subjectCommonName', 'no data') or 'no data')
                    ae.addAdditionalField('subjectCountry', 'Subject Country', each_cert.get('subjectCountry', 'no data') or 'no data')
                    ae.addAdditionalField('issuerCommonName', 'Issuer Common Name',
                                          each_cert.get('issuerCommonName', 'no data'))
                    ae.addAdditionalField('issuerCountry', 'Issuer Country', each_cert.get('issuerCountry', 'no data') or 'no data')
                    ae.addAdditionalField('issueDate', 'Issue Date',
                                          parse(each_cert.get('issueDate', '')).strftime("%Y-%m-%d") or 'no data')
                    ae.addAdditionalField('expirationDate', 'Expiration Date',
                                          parse(each_cert.get('expirationDate', '')).strftime("%Y-%m-%d") or 'no data')
                    ae.addAdditionalField('firstSeen', 'First Seen', datetime.datetime.fromtimestamp(
                        each_cert.get('firstSeen', '') // 1000).strftime("%Y-%m-%d") or 'no data')
                    ae.addAdditionalField('lastSeen', 'Last Seen', datetime.datetime.fromtimestamp(
                        each_cert.get('lastSeen', '') // 1000).strftime("%Y-%m-%d") or 'no data')
                    # ae.addAdditionalField('firstSeen', 'First Resolved', '%s' % each_cert['firstSeen'])
                    # ae.addAdditionalField('lastSeen', 'Last Resolved', '%s' % each_cert['lastSeen'])
        else:
            response.raise_for_status()
    except requests.exceptions.HTTPError as e:
        status_code = e.response.status_code
        if status_code == 401:
            at.addMessage('WARNING', 'Your credentials have expired')
        elif status_code == 500:
            at.addMessage('WARNING', 'The server encountered an unexpected error')
        else:
            at.addMessage('WARNING', 'Unknown error occurred, please try again')
    except:
        at.addException('domainToSSL Unknown Error:%sType: %s%sValue:%s' %
                        (os.linesep, sys.exc_info()[0], os.linesep, sys.exc_info()[1]))
    return at


def domainToSSLHistory(at, search_string):
    try:
        data = {'query': search_string}
        response = requests.get(url=api_base + 'ssl-certificate/search/keyword',
                                auth=(username, key), json=data)
        response_json = response.json()
        resp_code = response.status_code
        if resp_code == 200:
            if 'results' in response_json:
                for each_cert in response_json['results']:
                    at.addEntity(EntityTypes.Hash, '%s' % each_cert.get('focusPoint', 'no data') or 'no data')
        else:
            response.raise_for_status()
    except requests.exceptions.HTTPError as e:
        status_code = e.response.status_code
        if status_code == 401:
            at.addMessage('WARNING', 'Your credentials have expired')
        elif status_code == 500:
            at.addMessage('WARNING', 'The server encountered an unexpected error')
        else:
            at.addMessage('WARNING', 'Unknown error occurred, please try again')
    except:
        at.addException('domainToSSL Unknown Error:%sType: %s%sValue:%s' %
                        (os.linesep, sys.exc_info()[0], os.linesep, sys.exc_info()[1]))
    return at


def ipToSSLHistory(at, search_string):
    try:
        data = {'query': search_string}
        response = requests.get(url=api_base + 'ssl-certificate/history',
                                auth=(username, key), json=data)
        response_json = response.json()
        resp_code = response.status_code
        if resp_code == 200:
            if 'results' in response_json:
                for each_cert in response_json['results']:
                    ae = at.addEntity(EntityTypes.Hash, '%s' % each_cert.get('sha1', 'no data') or 'no data')
                    ae.addAdditionalField('firstSeen', 'First Seen', each_cert.get('firstSeen', 'no data') or 'no data')
                    ae.addAdditionalField('lastSeen', 'Last Seen', each_cert.get('lastSeen', 'no data') or 'no data')
        else:
            response.raise_for_status()
    except requests.exceptions.HTTPError as e:
        status_code = e.response.status_code
        if status_code == 401:
            at.addMessage('WARNING', 'Your credentials have expired')
        elif status_code == 500:
            at.addMessage('WARNING', 'The server encountered an unexpected error')
        else:
            at.addMessage('WARNING', 'Unknown error occurred, please try again')
    except:
        at.addException('domainToSSL Unknown Error:%sType: %s%sValue:%s' %
                        (os.linesep, sys.exc_info()[0], os.linesep, sys.exc_info()[1]))
    return at


def hashToSSL(at, search_string):
    try:
        data = {'query': search_string}
        response = requests.get(url=api_base + 'ssl-certificate',
                                auth=(username, key), json=data)
        response_json = response.json()
        resp_code = response.status_code
        if resp_code == 200:
            if 'results' in response_json:
                for each_cert in response_json['results']:
                    ae = at.addEntity(EntityTypes.Domain, '%s' % each_cert.get('subjectCommonName', 'no data') or 'no data')
                    ae.addAdditionalField('subjectCommonName', 'Subject Common Name', each_cert.get('subjectCommonName','no data') or 'no data')
                    ae.addAdditionalField('subjectCountry', 'Subject Country', each_cert.get('subjectCountry', 'no data'))
                    ae.addAdditionalField('issuerCommonName', 'Issuer Common Name', each_cert.get('issuerCommonName', 'no data') or 'no data')
                    ae.addAdditionalField('issuerCountry', 'Issuer Country', each_cert.get('issuerCountry', 'no data') or 'no data')
                    ae.addAdditionalField('issueDate', 'Issue Date', parse(each_cert.get('issueDate', '')).strftime("%Y-%m-%d") or 'no data')
                    ae.addAdditionalField('expirationDate', 'Expiration Date', parse(each_cert.get('expirationDate', '')).strftime("%Y-%m-%d") or 'no data')
                    ae.addAdditionalField('firstSeen', 'First Seen', datetime.datetime.fromtimestamp(each_cert.get('firstSeen', '') // 1000).strftime("%Y-%m-%d") or 'no data')
                    ae.addAdditionalField('lastSeen', 'Last Seen', datetime.datetime.fromtimestamp(each_cert.get('lastSeen', '') // 1000).strftime("%Y-%m-%d") or 'no data')
        else:
            response.raise_for_status()
    except requests.exceptions.HTTPError as e:
        status_code = e.response.status_code
        if status_code == 401:
            at.addMessage('WARNING', 'Your credentials have expired')
        elif status_code == 500:
            at.addMessage('WARNING', 'The server encountered an unexpected error')
        else:
            at.addMessage('WARNING', 'Unknown error occurred, please try again')
    except:
        at.addException('hashToSSL Unknown Error:%sType: %s%sValue:%s' %
                        (os.linesep, sys.exc_info()[0], os.linesep, sys.exc_info()[1]))
    return at


functions = {
    'domainToIP': domainToIP,
    'ipToDomain': ipToDomain,
    'emailToDomain': emailToDomain,
    'domainToChildHostPairs': domainToChildHostPairs,
    'domainToParentHostPairs': domainToParentHostPairs,
    'ipToSSL': ipToSSL,
    'domainToSSL': domainToSSL,
    'hashToSSL': hashToSSL,
    'domainToSSLHistory': domainToSSLHistory,
    'ipToSSLHistory': ipToSSLHistory
}


if __name__ == '__main__':
    at = AnomaliTransform()
    at.parseArguments()
    transform_name = at.getTransformName()
    entity_value = at.getEntityValue()
    key = at.getCredentialValue('api_key')
    username = at.getCredentialValue('username')

    functions[transform_name](at, entity_value)
    at.returnOutput()
