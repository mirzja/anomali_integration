#########################################################################
#
# Anomali CONFIDENTIAL
# __________________
#
#  Copyright 2016 Anomali Inc.
#  All Rights Reserved.
#
# NOTICE:  All information contained herein is, and remains
# the property of Anomali Incorporated and its suppliers,
# if any.  The intellectual and technical concepts contained
# herein are proprietary to Anomali Incorporated
# and its suppliers and may be covered by U.S. and Foreign Patents,
# patents in process, and are protected by trade secret or copyright law.
# Dissemination of this information or reproduction of this material
# is strictly forbidden unless prior written permission is obtained
# from Anomali Incorporated.
#
#########################################################################


import os
import sys
import requests
from dateutil.parser import parse
import datetime


from AnomaliEnrichment import AnomaliEnrichment, TextWidget, TableWidget, HorizontalRuleWidget, \
    ItemInWidget, ItemTypes, CompositeItem


api_base = "https://api.passivetotal.org/v2/"
username = None
key = None


def enrichWhoisIP(anomali_enrichment, search_string):
    try:
        data = {'query': search_string, 'compact_record': True, 'history': True}
        response = requests.get(url=api_base + 'whois?query=' + search_string, auth=(username, key),
                                json=data)
        response_json = response.json()
        resp_code = response.status_code
        if resp_code == 200:
            anomali_enrichment.addWidget(TextWidget(ItemInWidget(ItemTypes.Link,
                                                                 "https://community.riskiq.com/search/%s" % search_string,
                                                                 "View comparison in RiskIQ PassiveTotal")))
            # Create composite_item for TextWidget
            # always declare a new CompositeItem before using it
            text_composite_item = CompositeItem(onSeparateLines=False)
            whois_list = ['admin', 'billing', 'registrant', 'tech', 'nameServers', 'contactEmail', 'domain', 'expiresAt',
                         'registered', 'registrar', 'whoisServer', 'organization', 'name', 'telephone']

            # Create composite_item for TableWidget
            services_table_widget = TableWidget(columnHeadings=['Attribute', 'Value'],
                                                columnTypes=[ItemTypes.String, ItemTypes.String],
                                                columnWidths=['20%', '80%'])
            # In real world, get data from the API response
            for each_response in whois_list:
                result = response_json.get(each_response, None)
                if result is None:
                    continue

                # always declare a new CompositeItem before using it
                table_composite_item = CompositeItem(onSeparateLines=True)

                if type(result) == dict:
                    for line in result:
                        final_result = result[line]
                        table_composite_item.addItemInWidget(ItemInWidget(ItemTypes.String, final_result))
                elif type(result) == list:
                    for line in result:
                        final_result = line
                        table_composite_item.addItemInWidget(ItemInWidget(ItemTypes.String, final_result))
                else:
                    table_composite_item.addItemInWidget(ItemInWidget(ItemTypes.String, result))

                    # table_composite_item.addItemInWidget(ItemInWidget(ItemTypes.String, final_result))
                services_table_widget.addRowOfItems([ItemInWidget(ItemTypes.String, each_response),
                                                     table_composite_item])
            anomali_enrichment.addWidget(services_table_widget)
        else:
            response.raise_for_status()
    except requests.exceptions.HTTPError as e:
        status_code = e.response.status_code
        if status_code == 401:
            anomali_enrichment.addException('Your credentials have expired')
        elif status_code == 500:
            anomali_enrichment.addException('The server encountered an unexpected error')
        else:
            anomali_enrichment.addException('Unknown error occurred, please try again')
    except:
        anomali_enrichment.addException('enrichWhoisIP Unknown Error:%sType: %s%sValue:%s' %
                        (os.linesep, sys.exc_info()[0], os.linesep, sys.exc_info()[1]))
    return anomali_enrichment


def enrichWhoisDomain(anomali_enrichment, search_string):
    try:
        data = {'query': search_string, 'compact_record': True, 'history': True}
        response = requests.get(url=api_base + 'whois?query=' + search_string, auth=(username, key),
                                json=data)
        response_json = response.json()
        resp_code = response.status_code
        if resp_code == 200:
            anomali_enrichment.addWidget(TextWidget(ItemInWidget(ItemTypes.Link,
                                                                 "https://community.riskiq.com/search/%s" % search_string,
                                                                 "View comparison in RiskIQ PassiveTotal")))
            # Create composite_item for TextWidget
            # always declare a new CompositeItem before using it
            text_composite_item = CompositeItem(onSeparateLines=False)
            whois_list = ['admin', 'billing', 'registrant', 'tech', 'nameServers', 'contactEmail', 'domain', 'expiresAt',
                         'registered', 'registrar', 'whoisServer', 'organization', 'name', 'telephone']

            # Create composite_item for TableWidget
            services_table_widget = TableWidget(columnHeadings=['Attribute', 'Value'],
                                                columnTypes=[ItemTypes.String, ItemTypes.String],
                                                columnWidths=['20%', '80%'])
            # In real world, get data from the API response
            for each_response in whois_list:
                result = response_json.get(each_response, None)
                if result is None:
                    continue

                # always declare a new CompositeItem before using it
                table_composite_item = CompositeItem(onSeparateLines=True)

                if type(result) == dict:
                    for line in result:
                        final_result = result[line]
                        table_composite_item.addItemInWidget(ItemInWidget(ItemTypes.String, final_result))
                elif type(result) == list:
                    for line in result:
                        final_result = line
                        table_composite_item.addItemInWidget(ItemInWidget(ItemTypes.String, final_result))
                else:
                    table_composite_item.addItemInWidget(ItemInWidget(ItemTypes.String, result))

                    # table_composite_item.addItemInWidget(ItemInWidget(ItemTypes.String, final_result))
                services_table_widget.addRowOfItems([ItemInWidget(ItemTypes.String, each_response),
                                                     table_composite_item])
            anomali_enrichment.addWidget(services_table_widget)
        else:
            response.raise_for_status()
    except requests.exceptions.HTTPError as e:
        status_code = e.response.status_code
        if status_code == 401:
            anomali_enrichment.addException('Your credentials have expired')
        elif status_code == 500:
            anomali_enrichment.addException('The server encountered an unexpected error')
        else:
            anomali_enrichment.addException('Unknown error occurred, please try again')
    except:
        anomali_enrichment.addException('enrichWhoisDomain Unknown Error:%sType: %s%sValue:%s' %
                        (os.linesep, sys.exc_info()[0], os.linesep, sys.exc_info()[1]))
    return anomali_enrichment


def enrichDomain(anomali_enrichment, search_string):
    try:
        data = {'query': search_string}
        response = requests.get(url=api_base + 'dns/passive?query=' + search_string, auth=(username, key),
                                json=data)
        response_json = response.json()
        resp_code = response.status_code
        if resp_code == 200:
            anomali_enrichment.addWidget(TextWidget(ItemInWidget(ItemTypes.Link,
                                                                 "https://community.riskiq.com/search/%s" % search_string,
                                                                 "View comparison in RiskIQ PassiveTotal")))
            if 'results' in response_json:
                table_widget = TableWidget("Resolutions", ["Resolve", "First Seen", "Last Seen", "Link to RiskIQ PassiveTotal"])
                for resolution in response_json['results']:
                    recordType = resolution['recordType']
                    if recordType == 'A':
                        table_widget.addRowOfItems([ItemInWidget(ItemTypes.Link,
                                                                 "/detail/%s" % resolution['resolve'],
                                                                 resolution['resolve']),
                                                    ItemInWidget(ItemTypes.DateTime,
                                                                 resolution['firstSeen'],
                                                                 resolution['firstSeen']),
                                                    ItemInWidget(ItemTypes.DateTime,
                                                                 resolution['lastSeen'],
                                                                 resolution['lastSeen']),
                                                    ItemInWidget(ItemTypes.Link,
                                                                 "https://community.riskiq.com/search/%s" %
                                                                 resolution['resolve'], "View in RiskIQ PassiveTotal")])
                anomali_enrichment.addWidget(table_widget)

            # Create HorizontalRuleWidget
            anomali_enrichment.addWidget(HorizontalRuleWidget())
        else:
            response.raise_for_status()
    except requests.exceptions.HTTPError as e:
        status_code = e.response.status_code
        if status_code == 401:
            anomali_enrichment.addException('Your credentials have expired')
        elif status_code == 500:
            anomali_enrichment.addException('The server encountered an unexpected error')
        else:
            anomali_enrichment.addException('Unknown error occurred, please try again')
    except:
        anomali_enrichment.addException('enrichDomain Unknown Error:%sType: %s%sValue:%s' %
                        (os.linesep, sys.exc_info()[0], os.linesep, sys.exc_info()[1]))
    return anomali_enrichment


def enrichDNS(anomali_enrichment, search_string):
    try:
        data = {'query': search_string}
        response = requests.get(url=api_base + 'dns/passive?query=%s' % search_string, auth=(username, key),
                                json=data)
        response_json = response.json()
        resp_code = response.status_code
        if resp_code == 200:
            anomali_enrichment.addWidget(TextWidget(ItemInWidget(ItemTypes.Link,
                                                                 "https://community.riskiq.com/search/%s" % search_string,
                                                                 "View comparison in RiskIQ PassiveTotal")))
            if 'results' in response_json:
                table_widget = TableWidget("Resolutions", ["Resolve", "First Seen", "Last Seen", "Type", "Link to RiskIQ PassiveTotal"])
                for resolution in response_json['results']:
                    recordType = resolution['recordType']
                    if recordType != 'A':
                        type = ''
                        if recordType == 'NS':
                            asset_result = ItemTypes.NSRecord
                            asset_type = 'domain'
                        else:
                            asset_result = ItemTypes.Domain
                            asset_type = 'domain'

                        table_widget.addRowOfItems([ItemInWidget(ItemTypes.Link,
                                                                 "/detail/%s/%s" % (asset_type, resolution['resolve']),
                                                                 resolution['resolve']),
                                                    ItemInWidget(ItemTypes.DateTime,
                                                                 resolution['firstSeen'],
                                                                 resolution['firstSeen']),
                                                    ItemInWidget(ItemTypes.DateTime,
                                                                 resolution['lastSeen'],
                                                                 resolution['lastSeen']),
                                                    ItemInWidget(ItemTypes.String,
                                                                 resolution['recordType'],
                                                                 resolution['recordType']),
                                                    ItemInWidget(ItemTypes.Link,
                                                                 "https://community.riskiq.com/search/%s" %
                                                                 resolution['resolve'], "View in RiskIQ PassiveTotal")])
                anomali_enrichment.addWidget(table_widget)

            # Create HorizontalRuleWidget
            anomali_enrichment.addWidget(HorizontalRuleWidget())
        else:
            response.raise_for_status()
    except requests.exceptions.HTTPError as e:
        status_code = e.response.status_code
        if status_code == 401:
            anomali_enrichment.addException('Your credentials have expired')
        elif status_code == 500:
            anomali_enrichment.addException('The server encountered an unexpected error')
        else:
            anomali_enrichment.addException('Unknown error occurred, please try again')
    except:
        anomali_enrichment.addException('enrichDNS Unknown Error:%sType: %s%sValue:%s' %
                        (os.linesep, sys.exc_info()[0], os.linesep, sys.exc_info()[1]))
    return anomali_enrichment


def domain_to_trackers(anomali_enrichment, search_string):
    try:
        data = {'query': search_string}
        response = requests.get(url=api_base + 'host-attributes/trackers?query=%s' % search_string,
                                auth=(username, key), json=data)
        response_json = response.json()
        resp_code = response.status_code
        if resp_code == 200:
            anomali_enrichment.addWidget(TextWidget(ItemInWidget(ItemTypes.Link,
                                                                 "https://community.riskiq.com/search/%s" % search_string,
                                                                 "View comparison in RiskIQ PassiveTotal")))
            if 'results' in response_json:
                table_widget = TableWidget("Trackers", ["Hostname", "First Seen", "Last Seen", "Type", "Value",
                                                        "Link to RiskIQ PassiveTotal"])
                for each_tracker in response_json['results']:
                    table_widget.addRowOfItems([ItemInWidget(ItemTypes.Link,
                                                                 "/detail/domain/%s" % each_tracker['hostname'],
                                                                 each_tracker['hostname']),
                                                    ItemInWidget(ItemTypes.DateTime,
                                                                 each_tracker['firstSeen'],
                                                                 each_tracker['firstSeen']),
                                                    ItemInWidget(ItemTypes.DateTime,
                                                                 each_tracker['lastSeen'],
                                                                 each_tracker['lastSeen']),
                                                    ItemInWidget(ItemTypes.String,
                                                                 each_tracker['attributeType'],
                                                                 each_tracker['attributeType']),
                                                    ItemInWidget(ItemTypes.String,
                                                             each_tracker['attributeValue'],
                                                             each_tracker['attributeValue']),
                                                    ItemInWidget(ItemTypes.Link,
                                                                 "https://community.riskiq.com/search/trackers/%s" %
                                                                 each_tracker['attributeValue'],
                                                                 "View in RiskIQ PassiveTotal")])
                anomali_enrichment.addWidget(table_widget)

            # Create HorizontalRuleWidget
            anomali_enrichment.addWidget(HorizontalRuleWidget())
        else:
            response.raise_for_status()
    except requests.exceptions.HTTPError as e:
        status_code = e.response.status_code
        if status_code == 401:
            anomali_enrichment.addException('Your credentials have expired')
        elif status_code == 500:
            anomali_enrichment.addException('The server encountered an unexpected error')
        else:
            anomali_enrichment.addException('Unknown error occurred, please try again')
    except:
        anomali_enrichment.addException('domain_to_trackers Unknown Error:%sType: %s%sValue:%s' %
                        (os.linesep, sys.exc_info()[0], os.linesep, sys.exc_info()[1]))
    return anomali_enrichment


def domain_to_components(anomali_enrichment, search_string):
    try:
        data = {'query': search_string}
        response = requests.get(url=api_base + 'host-attributes/components?query=%s' % search_string,
                                auth=(username, key), json=data)
        response_json = response.json()
        resp_code = response.status_code
        if resp_code == 200:
            anomali_enrichment.addWidget(TextWidget(ItemInWidget(ItemTypes.Link,
                                                                 "https://community.riskiq.com/search/%s" % search_string,
                                                                 "View comparison in RiskIQ PassiveTotal")))
            if 'results' in response_json:
                table_widget = TableWidget("Components", ["Hostname", "First Seen", "Last Seen", "Category", "Value",
                                                        "Link to RiskIQ PassiveTotal"])
                for each_tracker in response_json['results']:
                    if each_tracker['version'] is None:
                        version = ''
                    else:
                        version = each_tracker['version']
                    if each_tracker['label'] is None:
                        label = ''
                    else:
                        label = each_tracker['label']
                    table_widget.addRowOfItems([ItemInWidget(ItemTypes.Link,
                                                             "/detail/domain/%s" % each_tracker['hostname'],
                                                             each_tracker['hostname']),
                                                    ItemInWidget(ItemTypes.DateTime,
                                                                 each_tracker['firstSeen'],
                                                                 each_tracker['firstSeen']),
                                                    ItemInWidget(ItemTypes.DateTime,
                                                                 each_tracker['lastSeen'],
                                                                 each_tracker['lastSeen']),
                                                    ItemInWidget(ItemTypes.String,
                                                                 each_tracker['category'],
                                                                 each_tracker['category']),
                                                    ItemInWidget(ItemTypes.String,
                                                             "%s %s" % (label, version),
                                                             "%s %s" % (label, version)),
                                                    ItemInWidget(ItemTypes.Link,
                                                                 "https://community.riskiq.com/search/%s/components" %
                                                                 each_tracker['hostname'],
                                                                 "View in RiskIQ PassiveTotal")])
                anomali_enrichment.addWidget(table_widget)

            # Create HorizontalRuleWidget
            anomali_enrichment.addWidget(HorizontalRuleWidget())
        else:
            response.raise_for_status()
    except requests.exceptions.HTTPError as e:
        status_code = e.response.status_code
        if status_code == 401:
            anomali_enrichment.addException('Your credentials have expired')
        elif status_code == 500:
            anomali_enrichment.addException('The server encountered an unexpected error')
        else:
            anomali_enrichment.addException('Unknown error occurred, please try again')
    except:
        anomali_enrichment.addException('domain_to_components Unknown Error:%sType: %s%sValue:%s' %
                        (os.linesep, sys.exc_info()[0], os.linesep, sys.exc_info()[1]))
    return anomali_enrichment


def domain_to_hostpairs(anomali_enrichment, search_string):
    try:
        data = []
        response = requests.get(
            url=api_base + 'host-attributes/pairs?query=%s&direction=children' % search_string,
            auth=(username, key))
        response_json = response.json()
        resp_code = response.status_code
        if resp_code == 200:
            for each_pair in response_json['results']:
                data.append(each_pair)

        response = requests.get(
            url=api_base + 'host-attributes/pairs?query=%s&direction=parents' % search_string,
            auth=(username, key))
        response_json = response.json()
        resp_code = response.status_code
        if resp_code == 200:
            for each_pair in response_json['results']:
                data.append(each_pair)

        if len(data) > 0:
            anomali_enrichment.addWidget(TextWidget(ItemInWidget(ItemTypes.Link,
                                                                 "https://community.riskiq.com/search/%s" % search_string,
                                                                 "View comparison in RiskIQ PassiveTotal")))
            table_widget = TableWidget("Host Pairs", ["Parent Hostname", "Child Hostname", "First Seen", "Last Seen", "Cause",
                                                    "Link to RiskIQ PassiveTotal"])
            for each_data_pair in data:
                table_widget.addRowOfItems([ItemInWidget(ItemTypes.Link,
                                                         "/detail/domain/%s" % each_data_pair['parent'],
                                                         each_data_pair['parent']),
                                                ItemInWidget(ItemTypes.Link,
                                                         "/detail/%s" % each_data_pair['child'],
                                                         each_data_pair['child']),
                                                ItemInWidget(ItemTypes.DateTime,
                                                             each_data_pair['firstSeen'],
                                                             each_data_pair['firstSeen']),
                                                ItemInWidget(ItemTypes.DateTime,
                                                             each_data_pair['lastSeen'],
                                                             each_data_pair['lastSeen']),
                                                ItemInWidget(ItemTypes.String,
                                                             each_data_pair['cause'],
                                                             each_data_pair['cause']),
                                                ItemInWidget(ItemTypes.Link,
                                                             "https://community.riskiq.com/search/%s/hostpairs" %
                                                             search_string,
                                                             "View in RiskIQ PassiveTotal")])
            anomali_enrichment.addWidget(table_widget)

            # Create HorizontalRuleWidget
            anomali_enrichment.addWidget(HorizontalRuleWidget())
        else:
            response.raise_for_status()
    except requests.exceptions.HTTPError as e:
        status_code = e.response.status_code
        if status_code == 401:
            anomali_enrichment.addException('Your credentials have expired')
        elif status_code == 500:
            anomali_enrichment.addException('The server encountered an unexpected error')
        else:
            anomali_enrichment.addException('Unknown error occurred, please try again')
    except:
        anomali_enrichment.addException('domain_to_hostpairs Unknown Error:%sType: %s%sValue:%s' %
                        (os.linesep, sys.exc_info()[0], os.linesep, sys.exc_info()[1]))
    return anomali_enrichment


def domain_to_osint(anomali_enrichment, search_string):
    try:
        response = requests.get(url=api_base + 'enrichment/osint?query=%s' % search_string,
                                auth=(username, key))
        response_json = response.json()
        resp_code = response.status_code
        if resp_code == 200:
            anomali_enrichment.addWidget(TextWidget(ItemInWidget(ItemTypes.Link,
                                                                 "https://community.riskiq.com/search/%s" % search_string,
                                                                 "View comparison in RiskIQ PassiveTotal")))
            if 'results' in response_json:
                table_widget = TableWidget("OSINT", ["Source", "Link", "Tags", "Link to RiskIQ PassiveTotal"])
                for each_osint in response_json['results']:
                    table_widget.addRowOfItems([ItemInWidget(ItemTypes.String,
                                                                 each_osint['source'],
                                                                 each_osint['source']),
                                                    ItemInWidget(ItemTypes.Link,
                                                                 each_osint['sourceUrl'],
                                                                 each_osint['sourceUrl']),
                                                    ItemInWidget(ItemTypes.String,
                                                                 ", ".join(each_osint['tags']),
                                                                 ", ".join(each_osint['tags'])),
                                                    ItemInWidget(ItemTypes.Link,
                                                                 "https://community.riskiq.com/search/%s/osint" %
                                                                 search_string,
                                                                 "View in RiskIQ PassiveTotal")])
                anomali_enrichment.addWidget(table_widget)

            # Create HorizontalRuleWidget
            anomali_enrichment.addWidget(HorizontalRuleWidget())
        else:
            response.raise_for_status()
    except requests.exceptions.HTTPError as e:
        status_code = e.response.status_code
        if status_code == 401:
            anomali_enrichment.addException('Your credentials have expired')
        elif status_code == 500:
            anomali_enrichment.addException('The server encountered an unexpected error')
        else:
            anomali_enrichment.addException('Unknown error occurred, please try again')
    except:
        anomali_enrichment.addException('domain_to_osint Unknown Error:%sType: %s%sValue:%s' %
                        (os.linesep, sys.exc_info()[0], os.linesep, sys.exc_info()[1]))
    return anomali_enrichment


def email_to_domain(anomali_enrichment, search_string):
    try:
        response = requests.get(url=api_base + 'whois/search?query=%s&field=email' % search_string,
                                auth=(username, key))
        response_json = response.json()
        resp_code = response.status_code
        if resp_code == 200:
            anomali_enrichment.addWidget(TextWidget(ItemInWidget(ItemTypes.Link,
                                                                 "https://community.riskiq.com/whois/email/%s" %
                                                                 search_string,
                                                                 "View comparison in RiskIQ PassiveTotal")))
            if 'results' in response_json:
                table_widget = TableWidget("Whois", ["Email", "Domain", "Link to RiskIQ PassiveTotal"])
                for each_record in response_json['results']:
                    table_widget.addRowOfItems([ItemInWidget(ItemTypes.Link,
                                                             "/detail/email/%s" % each_record['contactEmail'],
                                                             each_record['contactEmail']),
                                                    ItemInWidget(ItemTypes.Link,
                                                             "/detail/%s" % each_record['domain'],
                                                             each_record['domain']),
                                                    ItemInWidget(ItemTypes.Link,
                                                                 "https://community.riskiq.com/whois/email/%s" %
                                                                 search_string,
                                                                 "View in RiskIQ PassiveTotal")])
                anomali_enrichment.addWidget(table_widget)

            # Create HorizontalRuleWidget
            anomali_enrichment.addWidget(HorizontalRuleWidget())
        else:
            response.raise_for_status()
    except requests.exceptions.HTTPError as e:
        status_code = e.response.status_code
        if status_code == 401:
            anomali_enrichment.addException('Your credentials have expired')
        elif status_code == 500:
            anomali_enrichment.addException('The server encountered an unexpected error')
        else:
            anomali_enrichment.addException('Unknown error occurred, please try again')
    except:
        anomali_enrichment.addException('email_to_domain Unknown Error:%sType: %s%sValue:%s' %
                        (os.linesep, sys.exc_info()[0], os.linesep, sys.exc_info()[1]))
    return anomali_enrichment


def ip_to_components(anomali_enrichment, search_string):
    try:
        data = {'query': search_string}
        response = requests.get(url=api_base + 'host-attributes/components?query=%s' % search_string,
                                auth=(username, key), json=data)
        response_json = response.json()
        resp_code = response.status_code
        if resp_code == 200:
            anomali_enrichment.addWidget(TextWidget(ItemInWidget(ItemTypes.Link,
                                                                 "https://community.riskiq.com/search/%s" % search_string,
                                                                 "View comparison in RiskIQ PassiveTotal")))
            if 'results' in response_json:
                table_widget = TableWidget("Components", ["Address", "First Seen", "Last Seen", "Category", "Value",
                                                        "Link to RiskIQ PassiveTotal"])
                for each_tracker in response_json['results']:
                    if each_tracker['version'] is None:
                        version = ''
                    else:
                        version = each_tracker['version']
                    if each_tracker['label'] is None:
                        label = ''
                    else:
                        label = each_tracker['label']
                    table_widget.addRowOfItems([ItemInWidget(ItemTypes.Link,
                                                             "/detail/%s" % each_tracker['address'],
                                                             each_tracker['address']),
                                                    ItemInWidget(ItemTypes.DateTime,
                                                                 each_tracker['firstSeen'],
                                                                 each_tracker['firstSeen']),
                                                    ItemInWidget(ItemTypes.DateTime,
                                                                 each_tracker['lastSeen'],
                                                                 each_tracker['lastSeen']),
                                                    ItemInWidget(ItemTypes.String,
                                                                 each_tracker['category'],
                                                                 each_tracker['category']),
                                                    ItemInWidget(ItemTypes.String,
                                                             "%s %s" % (label, version),
                                                             "%s %s" % (label, version)),
                                                    ItemInWidget(ItemTypes.Link,
                                                                 "https://community.riskiq.com/search/%s/components" %
                                                                 each_tracker['address'],
                                                                 "View in RiskIQ PassiveTotal")])
                anomali_enrichment.addWidget(table_widget)
            # Create HorizontalRuleWidget
            anomali_enrichment.addWidget(HorizontalRuleWidget())
        else:
            response.raise_for_status()
    except requests.exceptions.HTTPError as e:
        status_code = e.response.status_code
        if status_code == 401:
            anomali_enrichment.addException('Your credentials have expired')
        elif status_code == 500:
            anomali_enrichment.addException('The server encountered an unexpected error')
        else:
            anomali_enrichment.addException('Unknown error occurred, please try again')
    except:
        anomali_enrichment.addException('ip_to_components Unknown Error:%sType: %s%sValue:%s' %
                        (os.linesep, sys.exc_info()[0], os.linesep, sys.exc_info()[1]))
    return anomali_enrichment


def ip_to_trackers(anomali_enrichment, search_string):
    try:
        data = {'query': search_string}
        response = requests.get(url=api_base + 'host-attributes/trackers?query=%s' % search_string,
                                auth=(username, key), json=data)
        response_json = response.json()
        resp_code = response.status_code
        if resp_code == 200:
            anomali_enrichment.addWidget(TextWidget(ItemInWidget(ItemTypes.Link,
                                                                 "https://community.riskiq.com/search/%s" % search_string,
                                                                 "View comparison in RiskIQ PassiveTotal")))
            if 'results' in response_json:
                table_widget = TableWidget("Trackers", ["Hostname", "First Seen", "Last Seen", "Type", "Value",
                                                        "Link to RiskIQ PassiveTotal"])
                for each_tracker in response_json['results']:
                    table_widget.addRowOfItems([ItemInWidget(ItemTypes.Link,
                                                             "/detail/ip/%s" % each_tracker['address'],
                                                             each_tracker['address']),
                                                    ItemInWidget(ItemTypes.DateTime,
                                                                 each_tracker['firstSeen'],
                                                                 each_tracker['firstSeen']),
                                                    ItemInWidget(ItemTypes.DateTime,
                                                                 each_tracker['lastSeen'],
                                                                 each_tracker['lastSeen']),
                                                    ItemInWidget(ItemTypes.String,
                                                                 each_tracker['attributeType'],
                                                                 each_tracker['attributeType']),
                                                    ItemInWidget(ItemTypes.String,
                                                             each_tracker['attributeValue'],
                                                             each_tracker['attributeValue']),
                                                    ItemInWidget(ItemTypes.Link,
                                                                 "https://community.riskiq.com/search/trackers/%s" %
                                                                 each_tracker['attributeValue'],
                                                                 "View in RiskIQ PassiveTotal")])
                anomali_enrichment.addWidget(table_widget)

            # Create HorizontalRuleWidget
            anomali_enrichment.addWidget(HorizontalRuleWidget())
        else:
            response.raise_for_status()
    except requests.exceptions.HTTPError as e:
        status_code = e.response.status_code
        if status_code == 401:
            anomali_enrichment.addException('Your credentials have expired')
        elif status_code == 500:
            anomali_enrichment.addException('The server encountered an unexpected error')
        else:
            anomali_enrichment.addException('Unknown error occurred, please try again')
    except:
        anomali_enrichment.addException('ip_to_trackers Unknown Error:%sType: %s%sValue:%s' %
                        (os.linesep, sys.exc_info()[0], os.linesep, sys.exc_info()[1]))
    return anomali_enrichment


def ip_to_osint(anomali_enrichment, search_string):
    try:
        response = requests.get(url=api_base + 'enrichment/osint?query=%s' % search_string,
                                auth=(username, key))
        response_json = response.json()
        resp_code = response.status_code
        if resp_code == 200:
            anomali_enrichment.addWidget(TextWidget(ItemInWidget(ItemTypes.Link,
                                                                 "https://community.riskiq.com/search/%s" % search_string,
                                                                 "View comparison in RiskIQ PassiveTotal")))
            if 'results' in response_json:
                table_widget = TableWidget("OSINT", ["Source", "Link", "Tags", "Link to RiskIQ PassiveTotal"])
                for each_osint in response_json['results']:
                    table_widget.addRowOfItems([ItemInWidget(ItemTypes.String,
                                                                 each_osint['source'],
                                                                 each_osint['source']),
                                                    ItemInWidget(ItemTypes.Link,
                                                                 each_osint['sourceUrl'],
                                                                 each_osint['sourceUrl']),
                                                    ItemInWidget(ItemTypes.String,
                                                                 ", ".join(each_osint['tags']),
                                                                 ", ".join(each_osint['tags'])),
                                                    ItemInWidget(ItemTypes.Link,
                                                                 "https://community.riskiq.com/search/%s/osint" %
                                                                 search_string,
                                                                 "View in RiskIQ PassiveTotal")])
                anomali_enrichment.addWidget(table_widget)

            # Create HorizontalRuleWidget
            anomali_enrichment.addWidget(HorizontalRuleWidget())
        else:
            response.raise_for_status()
    except requests.exceptions.HTTPError as e:
        status_code = e.response.status_code
        if status_code == 401:
            anomali_enrichment.addException('Your credentials have expired')
        elif status_code == 500:
            anomali_enrichment.addException('The server encountered an unexpected error')
        else:
            anomali_enrichment.addException('Unknown error occurred, please try again')
    except:
        anomali_enrichment.addException('ip_to_osint Unknown Error:%sType: %s%sValue:%s' %
                        (os.linesep, sys.exc_info()[0], os.linesep, sys.exc_info()[1]))
    return anomali_enrichment


def domainToCert(anomali_enrichment, search_string):
    try:
        # data = {'query': search_string}
        data = {'query': search_string, 'field': 'SubjectCommonName'}
        response = requests.get(url=api_base + 'ssl-certificate/search',
                                auth=(username, key), json=data)
        response_json = response.json()
        resp_code = response.status_code
        if resp_code == 200:
            anomali_enrichment.addWidget(TextWidget(ItemInWidget(ItemTypes.Link,
                                                                 "https://community.riskiq.com/search/%s/domaincertificates"
                                                                 % search_string,
                                                                 "View comparison in RiskIQ PassiveTotal")))
            if 'results' in response_json:
                table_widget = TableWidget("SSL Certificate",
                                           ["SHA1", "First Seen", "Last Seen", "Link to RiskIQ PassiveTotal"])
                for each_ssl in response_json['results']:
                    table_widget.addRowOfItems([ItemInWidget(ItemTypes.Link,
                                                             " /detail/%s" % each_ssl.get('sha1', 'no data'),
                                                             each_ssl.get('sha1', 'no data')),
                                                ItemInWidget(ItemTypes.Date,
                                                             datetime.datetime.fromtimestamp(each_ssl.get('firstSeen', '') // 1000).strftime("%Y-%m-%d"),
                                                             datetime.datetime.fromtimestamp(each_ssl.get('firstSeen', '') // 1000).strftime("%Y-%m-%d")),
                                                ItemInWidget(ItemTypes.Date,
                                                             datetime.datetime.fromtimestamp(each_ssl.get('lastSeen', '') // 1000).strftime("%Y-%m-%d"),
                                                             datetime.datetime.fromtimestamp(each_ssl.get('lastSeen', '') // 1000).strftime("%Y-%m-%d")),
                                                ItemInWidget(ItemTypes.Link,
                                                             "https://community.riskiq.com/search/certificate/sha1/%s"
                                                             % each_ssl['sha1'],
                                                             "View in RiskIQ PassiveTotal")])
                anomali_enrichment.addWidget(table_widget)

            # Create HorizontalRuleWidget
            anomali_enrichment.addWidget(HorizontalRuleWidget())

            data = {'query': search_string}
            response = requests.get(url=api_base + 'ssl-certificate/history',
                                    auth=(username, key), json=data)
            response_json = response.json()
            resp_code = response.status_code
            if resp_code == 200:
                anomali_enrichment.addWidget(TextWidget(ItemInWidget(ItemTypes.Link,
                                                                     "https://community.riskiq.com/search/%s/domaincertificates"
                                                                     % search_string,
                                                                     "View comparison in RiskIQ PassiveTotal"), False))
                if 'results' in response_json:
                    table_widget = TableWidget("SSL Certificate",
                                               ["SHA1", "First Seen", "Last Seen", "Link to RiskIQ PassiveTotal"])
                    for each_ssl in response_json['results']:
                        table_widget.addRowOfItems([ItemInWidget(ItemTypes.Link,
                                                                 "/detail/%s" % each_ssl['sha1'],
                                                                 each_ssl['sha1']),
                                                    ItemInWidget(ItemTypes.Date,
                                                                 parse(each_ssl['firstSeen']).strftime("%Y-%m-%d"),
                                                                 parse(each_ssl['firstSeen']).strftime("%Y-%m-%d")),
                                                    ItemInWidget(ItemTypes.Date,
                                                                 parse(each_ssl['lastSeen']).strftime("%Y-%m-%d"),
                                                                 parse(each_ssl['lastSeen']).strftime("%Y-%m-%d")),
                                                    ItemInWidget(ItemTypes.Link,
                                                                 "https://community.riskiq.com/search/certificate/sha1/%s"
                                                                 % each_ssl['sha1'],
                                                                 "View in RiskIQ PassiveTotal")])
                    anomali_enrichment.addWidget(table_widget)

                # Create HorizontalRuleWidget
                anomali_enrichment.addWidget(HorizontalRuleWidget())



        else:
            response.raise_for_status()
    except requests.exceptions.HTTPError as e:
        status_code = e.response.status_code
        if status_code == 401:
            anomali_enrichment.addException('Your credentials have expired')
        elif status_code == 500:
            anomali_enrichment.addException('The server encountered an unexpected error')
        else:
            anomali_enrichment.addException('Unknown error occurred, please try again')
    except:
        anomali_enrichment.addException('domainToCert Unknown Error:%sType: %s%sValue:%s' %
                                        (os.linesep, sys.exc_info()[0], os.linesep, sys.exc_info()[1]))
    return anomali_enrichment


def ipToCert(anomali_enrichment, search_string):
    try:
        data = {'query': search_string}
        response = requests.get(url=api_base + 'ssl-certificate/history',
                                auth=(username, key), json=data)
        response_json = response.json()
        resp_code = response.status_code
        if resp_code == 200:
            anomali_enrichment.addWidget(TextWidget(ItemInWidget(ItemTypes.Link,
                                                                 "https://community.riskiq.com/search/%s/domaincertificates"
                                                                 % search_string,
                                                                 "View comparison in RiskIQ PassiveTotal")))
            if 'results' in response_json:
                table_widget = TableWidget("SSL Certificate",
                                           ["SHA1", "First Seen", "Last Seen", "Link to RiskIQ PassiveTotal"])
                for each_ssl in response_json['results']:
                    table_widget.addRowOfItems([ItemInWidget(ItemTypes.Link,
                                                             " /detail/%s" % each_ssl.get('sha1', 'no data'),
                                                             each_ssl.get('sha1', 'no data')),
                                                ItemInWidget(ItemTypes.Date,
                                                             each_ssl.get('firstSeen', ''),
                                                             each_ssl.get('firstSeen', '')),
                                                ItemInWidget(ItemTypes.Date,
                                                             each_ssl.get('lastSeen', ''),
                                                             each_ssl.get('lastSeen', '')),
                                                ItemInWidget(ItemTypes.Link,
                                                             "https://community.riskiq.com/search/certificate/sha1/%s"
                                                             % each_ssl['sha1'],
                                                             "View in RiskIQ PassiveTotal")])
                anomali_enrichment.addWidget(table_widget)

            # Create HorizontalRuleWidget
            anomali_enrichment.addWidget(HorizontalRuleWidget())
        else:
            response.raise_for_status()
    except requests.exceptions.HTTPError as e:
        status_code = e.response.status_code
        if status_code == 401:
            anomali_enrichment.addException('Your credentials have expired')
        elif status_code == 500:
            anomali_enrichment.addException('The server encountered an unexpected error')
        else:
            anomali_enrichment.addException('Unknown error occurred, please try again')
    except:
        anomali_enrichment.addException('ipToCert Unknown Error:%sType: %s%sValue:%s' %
                                        (os.linesep, sys.exc_info()[0], os.linesep, sys.exc_info()[1]))
    return anomali_enrichment


def enrichSubDomain(anomali_enrichment, search_string):
    try:
        data = {'query': search_string}
        response = requests.get(url=api_base + 'enrichment/subdomains?query=%s' % search_string,
                                auth=(username, key), json=data)
        response_json = response.json()
        resp_code = response.status_code
        if resp_code == 200:
            anomali_enrichment.addWidget(TextWidget(ItemInWidget(ItemTypes.Link,
                                                                 "https://community.riskiq.com/search/%s" % search_string,
                                                                 "View comparison in RiskIQ PassiveTotal")))
            table_widget = TableWidget("Subdomains", ["Subdomain", "Link to RiskIQ PassiveTotal"])
            all_subdomains = response_json['subdomains']
            for each_subd in all_subdomains:
                table_widget.addRowOfItems([ItemInWidget(ItemTypes.Link,
                                                         "/detail/domain/%s.%s" % (each_subd, response_json['queryValue']),
                                                         "%s.%s" % (each_subd, response_json['queryValue'])),
                                            ItemInWidget(ItemTypes.Link,
                                                         "https://community.riskiq.com/search/%s.%s" %
                                                         (each_subd, response_json['queryValue']),
                                                         "View in RiskIQ PassiveTotal")])
            anomali_enrichment.addWidget(table_widget)

            # Create HorizontalRuleWidget
            anomali_enrichment.addWidget(HorizontalRuleWidget())
        else:
            response.raise_for_status()
    except requests.exceptions.HTTPError as e:
        status_code = e.response.status_code
        if status_code == 401:
            anomali_enrichment.addException('Your credentials have expired')
        elif status_code == 500:
            anomali_enrichment.addException('The server encountered an unexpected error')
        else:
            anomali_enrichment.addException('Unknown error occurred, please try again')
    except:
        anomali_enrichment.addException('enrichSubDomain Unknown Error:%sType: %s%sValue:%s' %
                        (os.linesep, sys.exc_info()[0], os.linesep, sys.exc_info()[1]))
    return anomali_enrichment


def enrichIP(anomali_enrichment, search_string):
    try:
        data = {'query': search_string}
        response = requests.get(url=api_base + 'dns/passive?query=%s' % search_string, auth=(username, key),
                                json=data)
        response_json = response.json()
        resp_code = response.status_code
        if resp_code == 200:
            anomali_enrichment.addWidget(TextWidget(ItemInWidget(ItemTypes.Link,
                                                                 "https://community.riskiq.com/search/%s" % search_string,
                                                                 "View comparison in RiskIQ PassiveTotal")))
            if 'results' in response_json:
                table_widget = TableWidget("Resolutions", ["Resolve", "First Seen", "Last Seen", "Link to RiskIQ PassiveTotal"])
                for resolution in response_json['results']:
                    recordType = resolution['recordType']
                    if recordType == 'A':
                        table_widget.addRowOfItems([ItemInWidget(ItemTypes.Link,
                                                                 "/detail/domain/%s" % (resolution['resolve']),
                                                                 resolution['resolve']),
                                                    ItemInWidget(ItemTypes.DateTime,
                                                                 resolution['firstSeen'],
                                                                 resolution['firstSeen']),
                                                    ItemInWidget(ItemTypes.DateTime,
                                                                 resolution['lastSeen'],
                                                                 resolution['lastSeen']),
                                                    ItemInWidget(ItemTypes.Link,
                                                                 "https://community.riskiq.com/search/%s" %
                                                                 resolution['resolve'], "View in RiskIQ PassiveTotal")])
                anomali_enrichment.addWidget(table_widget)

            # Create HorizontalRuleWidget
            anomali_enrichment.addWidget(HorizontalRuleWidget())
        else:
            response.raise_for_status()
    except requests.exceptions.HTTPError as e:
        status_code = e.response.status_code
        if status_code == 401:
            anomali_enrichment.addException('Your credentials have expired')
        elif status_code == 500:
            anomali_enrichment.addException('The server encountered an unexpected error')
        else:
            anomali_enrichment.addException('Unknown error occurred, please try again')
    except:
        anomali_enrichment.addException('enrichIP Unknown Error:%sType: %s%sValue:%s' %
                        (os.linesep, sys.exc_info()[0], os.linesep, sys.exc_info()[1]))
    return anomali_enrichment


def domainToMalware(anomali_enrichment, search_string):
    try:
        response = requests.get(url=api_base + 'enrichment/malware?query=%s' % search_string,
                                auth=(username, key))
        response_json = response.json()
        resp_code = response.status_code
        if resp_code == 200:
            anomali_enrichment.addWidget(TextWidget(ItemInWidget(ItemTypes.Link,
                                                                 "https://community.riskiq.com/search/%s" % search_string,
                                                                 "View comparison in RiskIQ PassiveTotal")))
            if 'results' in response_json:
                table_widget = TableWidget("Resolutions", ["Source", "Sample", "Collection Date"])
                for eachHash in response_json['results']:
                    table_widget.addRowOfItems([ItemInWidget(ItemTypes.Domain,
                                                             eachHash['source'],
                                                             eachHash['source']),
                                                ItemInWidget(ItemTypes.Link,
                                                             "/detail/hash/%s" % (eachHash['sample']),
                                                             eachHash['sample']),
                                                ItemInWidget(ItemTypes.DateTime,
                                                             eachHash['collectionDate'],
                                                             eachHash['collectionDate']),
                                                ItemInWidget(ItemTypes.Link, eachHash['sourceUrl'], "View report")])
                anomali_enrichment.addWidget(table_widget)

            # Create HorizontalRuleWidget
            anomali_enrichment.addWidget(HorizontalRuleWidget())
        else:
            response.raise_for_status()
    except requests.exceptions.HTTPError as e:
        status_code = e.response.status_code
        if status_code == 401:
            anomali_enrichment.addException('Your credentials have expired')
        elif status_code == 500:
            anomali_enrichment.addException('The server encountered an unexpected error')
        else:
            anomali_enrichment.addException('Unknown error occurred, please try again')
    except:
        anomali_enrichment.addException('domainToMalware Unknown Error:%sType: %s%sValue:%s' %
                        (os.linesep, sys.exc_info()[0], os.linesep, sys.exc_info()[1]))
    return anomali_enrichment


def ipToMalware(anomali_enrichment, search_string):
    try:
        response = requests.get(url=api_base + 'enrichment/malware?query=%s' % search_string,
                                auth=(username, key))
        response_json = response.json()
        resp_code = response.status_code
        if resp_code == 200:
            anomali_enrichment.addWidget(TextWidget(ItemInWidget(ItemTypes.Link,
                                                                 "https://community.riskiq.com/search/%s" % search_string,
                                                                 "View comparison in RiskIQ PassiveTotal")))
            if 'results' in response_json:
                table_widget = TableWidget("Hashes", ["Source", "Sample", "Collection Date"])
                for eachHash in response_json['results']:
                    table_widget.addRowOfItems([ItemInWidget(ItemTypes.IPv4,
                                                             eachHash['source'],
                                                             eachHash['source']),
                                                ItemInWidget(ItemTypes.Link,
                                                             "/detail/%s" % eachHash['sample'],
                                                             eachHash['sample']),
                                                ItemInWidget(ItemTypes.DateTime,
                                                             eachHash['collectionDate'],
                                                             eachHash['collectionDate']),
                                                ItemInWidget(ItemTypes.Link, eachHash['sourceUrl'], "View report")])
                anomali_enrichment.addWidget(table_widget)

            # Create HorizontalRuleWidget
            anomali_enrichment.addWidget(HorizontalRuleWidget())
        else:
            response.raise_for_status()
    except requests.exceptions.HTTPError as e:
        status_code = e.response.status_code
        if status_code == 401:
            anomali_enrichment.addException('Your credentials have expired')
        elif status_code == 500:
            anomali_enrichment.addException('The server encountered an unexpected error')
        else:
            anomali_enrichment.addException('Unknown error occurred, please try again')
    except:
        anomali_enrichment.addException('ipToMalware Unknown Error:%sType: %s%sValue:%s' %
                        (os.linesep, sys.exc_info()[0], os.linesep, sys.exc_info()[1]))
    return anomali_enrichment


functions = {
    'enrichWhoisDomain': enrichWhoisDomain,
    'enrichDomain': enrichDomain,
    'enrichSubDomain': enrichSubDomain,
    'enrichDNS': enrichDNS,
    'domain_to_trackers': domain_to_trackers,
    'enrichIP': enrichIP,
    'ip_to_trackers': ip_to_trackers,
    'domainToMalware': domainToMalware,
    'ipToMalware': ipToMalware,
    'domain_to_components': domain_to_components,
    'ip_to_components': ip_to_components,
    'domain_to_hostpairs': domain_to_hostpairs,
    'domain_to_osint': domain_to_osint,
    'ip_to_osint': ip_to_osint,
    'email_to_domain': email_to_domain,
    'domainToCert': domainToCert,
    'ipToCert': ipToCert,
    'enrichWhoisIP': enrichWhoisIP
}


if __name__ == '__main__':
    anomali_enrichment = AnomaliEnrichment()
    anomali_enrichment.parseArguments()
    transform_name = anomali_enrichment.getTransformName()
    entity_value = anomali_enrichment.getEntityValue()
    key = anomali_enrichment.getCredentialValue('api_key')
    username = anomali_enrichment.getCredentialValue('username')

    functions[transform_name](anomali_enrichment, entity_value)
    anomali_enrichment.returnOutput()
