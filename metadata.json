{
    "title": "RiskIQ",
    "author": "RiskIQ",
    "license": "https://www.riskiq.com/terms-of-use/",
    "source_url": "https://api.passivetotal.org/v2/",
    "long_ description": "All PassiveTotal datasets are utilized in this Anomali integration.",
    "transform_set_name": "RiskIQ",
    "description": "This integration allows users to view additional enrichments from Risk IQ, such as Passive SSL, on Observable Details Pages and pivot from Domains to Certificates on the Graph UI.",
    "icons": {
        "icon_display": "RiskIQ-logo.png",
        "icon_thumbnail": "thumbnail.png"
    },
    "sdk_type": "anomali",
    "app_name": "com.anomali.riskiq_enrichment",
    "version": "3.3.1",
    "credentials": [
        {
            "name": "username",
            "description": "API Key for RiskIQ app (RiskIQ API username)",
            "label": "API username",
            "required": true,
            "sensitive": true,
            "rank": 1
        },
        {
            "name": "api_key",
            "description": "API Token for RiskIQ app (RiskIQ API Key)",
            "label": "API Key",
            "required": true,
            "sensitive": true,
            "rank": 2
        }
    ],
    "transforms": [
        {
            "transform_name": "enrichWhoisDomain",
            "pivoting": false,
            "enrichment": true,
            "display_name": "Whois",
            "description": "Find all the Whois for the domain",
            "author": "RiskIQ",
            "parameters": "pt_anomali_enrichment.py enrichWhoisDomain",
            "entity_type": "anomali.Domain",
            "rank": 1
        },
        {
            "transform_name": "enrichDomain",
            "pivoting": false,
            "enrichment": true,
            "display_name": "Resolutions",
            "description": "Find all the enrichment for the domain",
            "author": "RiskIQ",
            "parameters": "pt_anomali_enrichment.py enrichDomain",
            "entity_type": "anomali.Domain",
            "rank": 2
        },
         {
            "transform_name": "enrichSubDomain",
            "pivoting": false,
            "enrichment": true,
            "display_name": "Subdomains",
            "description": "Find all the Subdomains for the domain",
            "author": "RiskIQ",
            "parameters": "pt_anomali_enrichment.py enrichSubDomain",
            "entity_type": "anomali.Domain",
            "rank": 3
        },
        {
            "transform_name": "enrichDNS",
            "pivoting": false,
            "enrichment": true,
            "display_name": "DNS",
            "description": "Find all DNS for the domain",
            "author": "RiskIQ",
            "parameters": "pt_anomali_enrichment.py enrichDNS",
            "entity_type": "anomali.Domain",
            "rank": 4
        },
        {
            "transform_name": "domain_to_trackers",
            "pivoting": false,
            "enrichment": true,
            "display_name": "Trackers",
            "description": "Find all trackers for the domain",
            "author": "RiskIQ",
            "parameters": "pt_anomali_enrichment.py domain_to_trackers",
            "entity_type": "anomali.Domain",
            "rank": 5
        },
        {
            "transform_name": "domain_to_components",
            "pivoting": false,
            "enrichment": true,
            "display_name": "Components",
            "description": "Find all components for the domain",
            "author": "RiskIQ",
            "parameters": "pt_anomali_enrichment.py domain_to_components",
            "entity_type": "anomali.Domain",
            "rank": 6
        },
        {
            "transform_name": "domain_to_hostpairs",
            "pivoting": false,
            "enrichment": true,
            "display_name": "Host Pairs",
            "description": "Find all host pairs for the domain",
            "author": "RiskIQ",
            "parameters": "pt_anomali_enrichment.py domain_to_hostpairs",
            "entity_type": "anomali.Domain",
            "rank": 7
        },
        {
            "transform_name": "domain_to_osint",
            "pivoting": false,
            "enrichment": true,
            "display_name": "OSINT",
            "description": "Find all OSINT for the domain",
            "author": "RiskIQ",
            "parameters": "pt_anomali_enrichment.py domain_to_osint",
            "entity_type": "anomali.Domain",
            "rank": 8
        },
        {
            "transform_name": "domainToMalware",
            "pivoting": false,
            "enrichment": true,
            "display_name": "Hashes",
            "description": "Find malware hashes seen on this IP Address",
            "author": "RiskIQ",
            "parameters": "pt_anomali_enrichment.py domainToMalware",
            "entity_type": "anomali.Domain",
            "rank": 9
        },
        {
            "transform_name": "domainToCert",
            "pivoting": false,
            "enrichment": true,
            "display_name": "SSL Certificate",
            "description": "Find all SSL Certificates by Common Name",
            "author": "RiskIQ",
            "parameters": "pt_anomali_enrichment.py domainToCert",
            "entity_type": "anomali.Domain",
            "rank": 10
        },
        {
            "transform_name": "email_to_domain",
            "pivoting": false,
            "enrichment": true,
            "display_name": "Email",
            "description": "Find all email for the domain",
            "author": "RiskIQ",
            "parameters": "pt_anomali_enrichment.py email_to_domain",
            "entity_type": "anomali.EmailAddress",
            "rank": 1
        },
        {
            "transform_name": "enrichWhoisIP",
            "pivoting": false,
            "enrichment": true,
            "display_name": "Whois",
            "description": "Find all the Whois for the IP Address",
            "author": "RiskIQ",
            "parameters": "pt_anomali_enrichment.py enrichWhoisIP",
            "entity_type": "anomali.IPv4Address",
            "rank": 1
        },
        {
            "transform_name": "enrichIP",
            "pivoting": false,
            "enrichment": true,
            "display_name": "Resolutions",
            "description": "Find all the enrichment for the IP address",
            "author": "RiskIQ",
            "parameters": "pt_anomali_enrichment.py enrichIP",
            "entity_type": "anomali.IPv4Address",
            "rank": 2
        },
        {
            "transform_name": "ip_to_trackers",
            "pivoting": false,
            "enrichment": true,
            "display_name": "Trackers",
            "description": "Find all trackers for the IP Address",
            "author": "RiskIQ",
            "parameters": "pt_anomali_enrichment.py ip_to_trackers",
            "entity_type": "anomali.IPv4Address",
            "rank": 3
        },
        {
            "transform_name": "ip_to_components",
            "pivoting": false,
            "enrichment": true,
            "display_name": "Components",
            "description": "Find all components for the IP Address",
            "author": "RiskIQ",
            "parameters": "pt_anomali_enrichment.py ip_to_components",
            "entity_type": "anomali.IPv4Address",
            "rank": 4
        },
        {
            "transform_name": "ip_to_osint",
            "pivoting": false,
            "enrichment": true,
            "display_name": "OSINT",
            "description": "Find all OSINT for the IP Address",
            "author": "RiskIQ",
            "parameters": "pt_anomali_enrichment.py ip_to_osint",
            "entity_type": "anomali.IPv4Address",
            "rank": 5
        },
        {
            "transform_name": "ipToMalware",
            "pivoting": false,
            "enrichment": true,
            "display_name": "Hashes",
            "description": "Find malware hashes seen on this IP Address",
            "author": "RiskIQ",
            "parameters": "pt_anomali_enrichment.py ipToMalware",
            "entity_type": "anomali.IPv4Address",
            "rank": 6
        },
        {
            "transform_name": "ipToCert",
            "pivoting": false,
            "enrichment": true,
            "display_name": "SSL Certificate",
            "description": "Find all SSL Certificates by Common Name",
            "author": "RiskIQ",
            "parameters": "pt_anomali_enrichment.py ipToCert",
            "entity_type": "anomali.IPv4Address",
            "rank": 7
        },
        {
            "transform_name": "domainToIP",
            "display_name": "Resolutions",
            "description": "Find all IPs which the domain can be resolved to",
            "author": "RiskIQ",
            "parameters": "pt_anomali_transform.py domainToIP",
            "entity_type": "anomali.Domain"
        },
        {
            "transform_name": "ipToDomain",
            "display_name": "Resolutions",
            "description": "Find all domains which the IP addresses can be resolved to",
            "author": "RiskIQ",
            "parameters": "pt_anomali_transform.py ipToDomain",
            "entity_type": "anomali.IPv4Address"
        },
        {
            "transform_name": "domainToChildHostPairs",
            "display_name": "Child Host Pairs",
            "description": "Find Child Host Pairs linked to the Domain",
            "author": "RiskIQ",
            "parameters": "pt_anomali_transform.py domainToChildHostPairs",
            "entity_type": "anomali.Domain"
        },
        {
            "transform_name": "domainToParentHostPairs",
            "display_name": "Parent Host Pairs",
            "description": "Find Parent Host Pairs linked to the Domain",
            "author": "RiskIQ",
            "parameters": "pt_anomali_transform.py domainToParentHostPairs",
            "entity_type": "anomali.Domain"
        },
        {
            "transform_name": "emailToDomain",
            "display_name": "Resolutions",
            "description": "Find artifacts linked to the email",
            "author": "RiskIQ",
            "parameters": "pt_anomali_transform.py emailToDomain",
            "entity_type": "anomali.EmailAddress"
        },
        {
            "transform_name": "ipToSSL",
            "display_name": "SSL by Common Name",
            "description": "Find SSL Certificates linked to the IP Address",
            "author": "RiskIQ",
            "parameters": "pt_anomali_transform.py ipToSSL",
            "entity_type": "anomali.IPv4Address"
        },
        {
            "transform_name": "domainToSSL",
            "display_name": "SSL by Common Name",
            "description": "Find SSL Certificates linked to the Domain by Common Name",
            "author": "RiskIQ",
            "parameters": "pt_anomali_transform.py domainToSSL",
            "entity_type": "anomali.Domain"
        },
        {
            "transform_name": "hashToSSL",
            "display_name": "SSL by Hash",
            "description": "Find SSL Certificate by the hash",
            "author": "RiskIQ",
            "parameters": "pt_anomali_transform.py hashToSSL",
            "entity_type": "anomali.Hash"
        }
    ]
}
